all:
	@echo "Use '$(MAKE) install' to install"

install:
	# usb tethering
	install -d $(DESTDIR)/$(PREFIX)/bin
	install -m 755 usb-tethering $(DESTDIR)/$(PREFIX)/bin/

	install -d $(DESTDIR)/lib/systemd/system/
	install -m 644 usb-tethering.service $(DESTDIR)/lib/systemd/system/

	install -d $(DESTDIR)/etc/init.d
	install -m 755 usb-tethering.init $(DESTDIR)/etc/init.d/

	# dhcpd configuration
	install -d $(DESTDIR)/$(PREFIX)/lib/tmpfiles.d/
	install -m 644 tmpfiles.d.hybris-usb.conf $(DESTDIR)/$(PREFIX)/lib/tmpfiles.d/hybris-usb.conf

	install -d $(DESTDIR)/etc/hybris-usb/
	install -m 644 dhcpd.conf $(DESTDIR)/etc/hybris-usb/

